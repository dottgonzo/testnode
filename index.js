var express = require('express')
var bodyParser = require('body-parser')




var app = express()


// QUESTO SERVE PER DELLE RICHIESTE CROSS-ORIGIN, TU METTILO NON SI SA MAI

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
}
);

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());


app.get('/', function (req, res) {

    console.log(req.query.dato1) // questo stampa il parametro dato1

    // nel get è req.params, nel post req.body

    res.send({ ok: true }) // questo è quello che vedi se visiti la pagina http://localhost:3000/

})


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
